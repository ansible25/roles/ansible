Ansible
=========

Role that installs Ansible and Molecule

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

- role: python3
- collection: bradthebuilder.virtualization

Example Playbook
----------------

Installing Ansible on targeted machine:

    - hosts: servers
      roles:
         - ansible

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
